﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GoTVisma.Models
{
    public class GameOfThrones
    {
        public string CharacterLink { get; set; }
        public string HouseName { get; set; }
        public string SwornMembers { get; set; }
    }
}