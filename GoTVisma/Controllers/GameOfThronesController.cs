﻿using GoTVisma.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GoTVisma.Controllers
{
    public class GameOfThronesController : Controller
    {
        // GET: GameOfThrones
        public ActionResult GoTBrowser()
        {
            var houses = new List<GameOfThrones>();
            var housesListAPI = GoTAPI.House.GetListOfHouses();
            foreach (var houseAPI in housesListAPI)
            {
                var house = new GameOfThrones();
                house.CharacterLink = houseAPI.url;
                house.HouseName = houseAPI.name;

                var swornMembersList = GoTAPI.Character.GetCharacterBySwornMembers(houseAPI.swornMembers);

                foreach (var swormMember in swornMembersList)
                    if (!string.IsNullOrEmpty(swormMember.name))
                        house.SwornMembers += "<li>" + swormMember.name.ToString() + "</li>";
                houses.Add(house);
            }

            return View(houses);
        }
    }
}